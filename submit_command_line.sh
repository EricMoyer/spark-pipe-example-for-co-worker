hdfs dfs -rm -r simple_test_output.txt
hdfs dfs -rm -r simple_test_lengths.txt
spark-submit --master yarn --deploy-mode cluster --py-files simple_test.py --files length_and_wait.pl --num-executors 25 simple_test.py >& simple_test.log
hdfs dfs -getmerge simple_test_output.txt so.txt
hdfs dfs -getmerge simple_test_lengths.txt sl.txt
