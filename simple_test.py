from pyspark import SparkContext, SparkConf
from time import sleep

def expand_and_wait(n):
    sleep(1)
    return "X"*n

conf = SparkConf().setAppName("Eric_Test_Spark")
sc = SparkContext(conf=conf)

# Make a text file (i.e. verify that normal RDD stuff parallelizes)
TEXT_NAME="simple_test_output.txt"

numbers = sc.parallelize(range(1,1000),50)
strings = numbers.map(expand_and_wait)
strings.saveAsTextFile(TEXT_NAME)

# Run that text file through an external script (i.e. verify that pipe parallelizes)
lines = sc.textFile(TEXT_NAME, minPartitions=50)  # minPartitions isn't necessary since it will already be 50 files, but I put it here for demonstration
lengths = lines.pipe("perl length_and_wait.pl")
lengths.saveAsTextFile("simple_test_lengths.txt")
